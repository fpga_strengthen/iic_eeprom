`timescale 1ns/1ps

module seq_logic();

    reg         clk;
    reg         rst_n;

    reg [7:0]   data_1;
    reg [7:0]   data_2;
    reg [7:0]   data_3;

    reg [1:0]   cnt_clk;
    reg [2:0]   cnt_bit;

    initial begin
        clk = 1'b0;
        rst_n <= 1'b0;
        #40
        rst_n <= 1'b1;
    end

    always #10 clk = ~clk;

    //cnt_clk：从0~3循环计数，计数周期是4
    always@(posedge clk or negedge rst_n)begin 
        if(rst_n == 1'b0)
            cnt_clk <= 2'd0;
        else
            cnt_clk <= cnt_clk + 2'd1;
    end
    //每当cnt_clk=3时cnt_bit加1，相当于对clk四分频；计数周期是8
    always@(posedge clk or negedge rst_n)begin 
        if(rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if((cnt_bit == 3'd7)&&(cnt_clk == 2'd3))
            cnt_bit <= 3'd0;
        else if(cnt_clk == 2'd3)
            cnt_bit <= cnt_bit + 3'd1;
        else
            cnt_bit <= cnt_bit;
    end

    //cnt_clk：从0~3循环计数
    //每当cnt_clk=3时cnt_bit加1,cnt_bit从0~7循环计数
    always@(*)begin
        if(rst_n == 1'b0)
            data_1 = 8'b1001_110z;
        else if(cnt_clk == 2'd2)
            data_1 = {data_1[6:0],1'bz};
        else
            data_1 = data_1;
    end

    always@(*)begin
        if(rst_n == 1'b0)
            data_2 <= 8'b1001_110z;
        else if(cnt_clk == 2'd2)
            data_2[7-cnt_bit] <= 1'bz;
        else
            data_2 <= data_2;
    end

    always@(*)begin
        if(rst_n == 1'b0)
            data_3 <= 8'b1001_110z;
        else if(cnt_clk == 2'd2)
            data_3 <= {data_3[6:0],1'bz};
        else
            data_3 <= data_3;
    end

endmodule