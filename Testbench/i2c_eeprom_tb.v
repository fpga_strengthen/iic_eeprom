`timescale 1ns/1ns
module i2c_eeprom_tb();

    reg     i_clk_50M;
    reg     i_rst_n;            //复位，低有效
    reg     i_key_wr;           //按键信号都是低有效
    reg     i_key_rd;

    wire    o_shcp;
    wire    o_stcp;
    wire    o_oe;
    wire    o_ds;
    wire    o_scl;
    wire    io_sda;

    //输入信号初始化
    initial begin 
        //初始化
        i_clk_50M = 1'b1;
        i_rst_n  <= 1'b0; 
        i_key_wr <= 1'b1;
        i_key_rd <= 1'b1;
        #200
        //拉高复位信号，并按下写操作按键
        i_rst_n  <= 1'b1;
        i_key_wr <= 1'b0;
        i_key_rd <= 1'b1;
        #400
        //写操作按键松开
        i_key_wr <= 1'b1;
        i_key_rd <= 1'b1;
        #5500000
        //等待够长的时间后，写操作结束，按下读操作按键
        i_key_wr <= 1'b1;
        i_key_rd <= 1'b0;
        #400
        //读操作按键松开
        i_key_wr <= 1'b1;
        i_key_rd <= 1'b1;
    end

    always #10 i_clk_50M = ~i_clk_50M;          //时钟，50MHz

    //参数重定义，缩短仿真时间
    defparam    U_i2c_eeprom.CNT_MAX = 20'd5;
    defparam    U_i2c_eeprom.U_i2c_rw_ctrl.MAX_CNT_DELAY = 1000;

    //顶层模块实例化
    i2c_eeprom_top U_i2c_eeprom(
        .i_clk_50M  (i_clk_50M),
        .i_rst_n    (i_rst_n),
        .i_key_wr   (i_key_wr),
        .i_key_rd   (i_key_rd),

        .o_shcp     (o_shcp),
        .o_stcp     (o_stcp),
        .o_oe       (o_oe  ),
        .o_ds       (o_ds  ),
        .o_scl      (o_scl ),
        .io_sda     (io_sda)
    );

    //EEPROM仿真模型
    M24LC64 U_M24LC64(
        .A0      (1'b1), 
        .A1      (1'b1), 
        .A2      (1'b0), 
        .WP      (1'b0),            //写保护

        .SDA     (io_sda), 
        .SCL     (o_scl), 
        .RESET   (~i_rst_n)         //仿真模型复位高有效
    );


endmodule