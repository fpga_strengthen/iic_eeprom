module seg_595_dynamic(
    sys_clk,
    sys_rst_n,
    data,
    point,
    sign,
    seg_en,
    ds,
    oe,
    shcp,
    stcp
);
    input   wire        sys_clk;        //时钟，50M
    input   wire        sys_rst_n;      //复位，低有效
    input   wire [19:0] data;           //数据
    input   wire [5:0]  point;          //小数点控制
    input   wire        sign;           //符号位
    input   wire        seg_en;         //数码管使能

    output  wire        ds;             //串行数据
    output  wire        oe;             //输出使能，低有效
    output  wire        shcp;           //移位寄存器时钟
    output  wire        stcp;           //存储寄存器时钟

    //中间连线和寄存器变量声明
    wire    [5:0]   sel;                //数码管位选信号
    wire    [7:0]   seg;                //数码管段选信号
    
    //调用数码管驱动模块
    seg_dynamic U_seg_dynamic(
        .sys_clk    (sys_clk),
        .sys_rst_n  (sys_rst_n),
        .point      (point),
        .seg_en     (seg_en),
        .data       (data),
        .sign       (sign),
        
        .sel        (sel),
        .seg        (seg)
    );

    //调用HC模块产生串行输出
    hc_595_ctrl U_hc_595_ctrl(
        .sys_clk    (sys_clk),
        .sys_rst_n  (sys_rst_n),
        .seg        (seg),
        .sel        (sel),
        
        .ds         (ds),
        .shcp       (shcp),
        .stcp       (stcp),
        .oe         (oe)
    );

endmodule