//对74HC595芯片的控制信号
module hc_595_ctrl(
    sys_clk,
    sys_rst_n,
    seg,
    sel,
    ds,
    shcp,
    stcp,
    oe
);
    input   wire        sys_clk;       //时钟,50M
    input   wire        sys_rst_n;     //复位信号,低有效
    input   wire [7:0]  seg;           //数码管段选信号
    input   wire [5:0]  sel;           //数码管位选信号

    output  reg         ds;            //串行输出数据
    output  reg         shcp;          //串行数据移位寄存器时钟
    output  reg         stcp;          //存储寄存器时钟
    output  reg         oe;            //输出使能信号，设置为常低

    //将并行数据转换为14位串行数据
    wire    [13:0]  data;
    assign  data = {seg[0],seg[1],seg[2],seg[3],seg[4],seg[5],seg[6],seg[7],sel};

    //对系统时钟进行四分频用于串行数据输出
    reg     [1:0]   cnt_clk;

    //计数范围0~3，四个数，计满时自动清零，得到时钟12.5M
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_clk <= 2'd0;
        else 
            cnt_clk <= cnt_clk + 1'b1;   //计满自动清零，无需对cnt_clk=3单独讨论
    end

    //串行数据输出计数，计数范围0~13，共14个数
    reg     [3:0]   cnt_ds;

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_ds <= 4'd0;
        else if((cnt_ds == 4'd13)&&(cnt_clk == 2'd3))
            cnt_ds <= 4'd0;
        else if(cnt_clk == 2'd3)
            cnt_ds <= cnt_ds + 1'b1;   //四分频计数器每计满一次就加一,这样该计数器的时钟其实也是12.5M
        else    
            cnt_ds <= cnt_ds;
    end

    //串行数据输出逻辑
/*
    //使用这样的转化方法的缺点是时钟是50M，会有多次赋值；如果换成12.5M的时钟会更好
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            ds <= 1'b0;
        else begin 
            case(cnt_ds)
                4'd0 : ds <= data[0];
                4'd1 : ds <= data[1];
                4'd2 : ds <= data[2];
                4'd3 : ds <= data[3];
                4'd4 : ds <= data[4];
                4'd5 : ds <= data[5];
                4'd6 : ds <= data[6];
                4'd7 : ds <= data[7];
                4'd8 : ds <= data[8];
                4'd9 : ds <= data[9];
                4'ha : ds <= data[10];
                4'hb : ds <= data[11];
                4'hc : ds <= data[12];
                4'hd : ds <= data[13];
                default : ds <= 1'b0; 
            endcase
        end
    end
*/

    //采用该输出方法,ds的时钟就是12.5M
    always @(posedge sys_clk or negedge sys_rst_n) begin
        if(!sys_rst_n)
            ds <= 1'b0;
        else if(cnt_clk == 2'd1)
            ds <= data[cnt_ds];
        else 
            ds <= ds;

    end

    //串行数据输出时钟
    //为了稳定，在每个数据的中间即cnt_clk=2时将shcp拉高
/*
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            shcp <= 1'b0;
        else if(cnt_clk == 2'd2)
            shcp <= ~shcp;    //这样写是错误的，产生的是8分频的时钟
        else
            shcp <= shcp;
    end
*/
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            shcp <= 1'b0;
        else if(cnt_clk > 2'd1)
            shcp <= 1'b1;
        else    
            shcp <= 1'b0;
    end

    //数据存储时钟信号stcp
    //每传输完14bit的串行数据就将该信号拉高一次
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            stcp <= 1'b0;
        else if((cnt_ds == 4'd13)&&(cnt_clk == 2'd3))
            stcp <= 1'b1;
        else
            stcp <= 1'b0;
    end

    //配置输出使能信号,设置为常低即可
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            oe <= 1'b1;
        else
            oe <= 1'b0;
    end
    
endmodule