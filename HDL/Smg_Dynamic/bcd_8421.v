//---------------------------------------------------------------------------
//-------sub-module 2
//-------Description:
//-------本模块的作用是将十进制的数据转换为8421BCD码，以方便在数码管上的显示
//---------------------------------------------------------------------------

module bcd_8421(
    sys_clk,
    sys_rst_n,
    data,
    unit,
    ten,
    hund,
    thou,
    t_thou,
    h_thou
);

    input   wire         sys_clk;        //时钟，50M
    input   wire         sys_rst_n;      //复位，低有效
    input   wire [19:0]  data;           //20位长的数据

    output  reg  [3:0]   unit;           //个位，因为每个数码管要表示的十进制数的范围是0~9，所以至少需要4位
    output  reg  [3:0]   ten;            //十位
    output  reg  [3:0]   hund;           //百位
    output  reg  [3:0]   thou;           //千位
    output  reg  [3:0]   t_thou;         //一万
    output  reg  [3:0]   h_thou;         //十万

    //-----------寄存器声明-------------//
    reg [4:0]   cnt_shift;              //移位计数器，总共需要移位20次
    reg [43:0]  data_shift;             //寄存移位过程中的数据，移位完成后输出
    reg         shift_flag;             //为低时进行判断，为高时进行移位

    //shift_flag的一个周期就可以完成一次判断和移位，所以需要20次就可以对data完成8421BCD码的转换
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            shift_flag <= 1'b0;
        else
            shift_flag <= ~shift_flag; 
    end

    //对移位操作进行计数；由于为0时是进行初始化，所以从1开始算移位，到20
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_shift <= 5'd0;
        else if((cnt_shift == 5'd21)&&(shift_flag == 1'b1))
            cnt_shift <= 5'd0;
        else if(shift_flag == 1'b1)
            cnt_shift <= cnt_shift + 5'd1;
        else
            cnt_shift <= cnt_shift;
    end

    //移位和判断逻辑
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            data_shift <= 44'b0;
        else if(cnt_shift == 5'd0)
            data_shift <= {{24{1'b0}},data};
        else if((shift_flag == 1'b0)&&(cnt_shift <= 5'd20))begin 
            data_shift[23:20] <= (data_shift[23:20] > 3'd4) ? (data_shift[23:20] + 2'd3) : data_shift[23:20];
            data_shift[27:24] <= (data_shift[27:24] > 3'd4) ? (data_shift[27:24] + 2'd3) : data_shift[27:24];
            data_shift[31:28] <= (data_shift[31:28] > 3'd4) ? (data_shift[31:28] + 2'd3) : data_shift[31:28];
            data_shift[35:32] <= (data_shift[35:32] > 3'd4) ? (data_shift[35:32] + 2'd3) : data_shift[35:32];
            data_shift[39:36] <= (data_shift[39:36] > 3'd4) ? (data_shift[39:36] + 2'd3) : data_shift[39:36];
            data_shift[43:40] <= (data_shift[43:40] > 3'd4) ? (data_shift[43:40] + 2'd3) : data_shift[43:40];
        end
        else if((shift_flag == 1'b1)&&(cnt_shift <= 5'd20))
            data_shift <= (data_shift << 1'b1);         //数据的组合主要在这里发生移位
        else
            data_shift <= data_shift;
    end

    //当累计移位20次后已经转换完成，在cnt_shift=21时输出移位结果
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)begin 
            unit   <= 4'b0;
            ten    <= 4'd0;
            hund   <= 4'd0;
            thou   <= 4'd0;
            t_thou <= 4'd0;
            h_thou <= 4'd0;
        end
        else if(cnt_shift == 5'd21)begin 
            unit   <= data_shift[23:20];
            ten    <= data_shift[27:24];
            hund   <= data_shift[31:28];
            thou   <= data_shift[35:32];
            t_thou <= data_shift[39:36];
            h_thou <= data_shift[43:40];            
        end
        else begin      //其他时刻保持不变，否则显示的数据会突变
            unit   <= unit;
            ten    <= ten;
            hund   <= hund;
            thou   <= thou;
            t_thou <= t_thou;
            h_thou <= h_thou;
        end
    end


endmodule