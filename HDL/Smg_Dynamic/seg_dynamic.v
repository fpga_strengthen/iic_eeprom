//---------------------------------------------------------------------------
//-------sub-module 3
//-------Description:
//-------本模块的作用是将输入的十进制数据转换为8421BCD码并进行显示
//-------注意BCD码模块会相对有21个时钟周期的延时
//---------------------------------------------------------------------------
module seg_dynamic(
    sys_clk,
    sys_rst_n,
    point,
    seg_en,
    data,
    sign,
    sel,
    seg
);
    input   wire        sys_clk;            //系统时钟，50M
    input   wire        sys_rst_n;          //系统复位，低有效
    input   wire [5:0]  point;              //小数点显示控制
    input   wire        seg_en;             //数码管使能信号
    input   wire [19:0] data;               //输入的十进制数据，待转换为bcd码
    input   wire        sign;               //符号位控制信号，高有效（对应负数）

    output  reg [5:0]   sel;                //数码管段选信号
    output  reg [7:0]   seg;                //数码管位选信号

    //定义1ms时间，用于数码管状态的刷新
    parameter CNT_1MS = 16'd49_999;        //1ms时间

    //由data转换得到的每位数据的BCD码
    wire    [3:0]   unit;       //个位
    wire    [3:0]   ten;        //十位
    wire    [3:0]   hund;       //百位
    wire    [3:0]   thou;       //千位
    wire    [3:0]   t_thou;     //万位
    wire    [3:0]   h_thou;     //十万位
       
    //实例化8421BCD码模块，根据数据产生相应的BCD码
    bcd_8421 U_bcd_8421(
        .sys_clk    (sys_clk),
        .sys_rst_n  (sys_rst_n),
        .data       (data),
        
        .unit       (unit),
        .ten        (ten),
        .hund       (hund),
        .thou       (thou),
        .t_thou     (t_thou),
        .h_thou     (h_thou)
    );
    
    //控制数据的每一位的显示判断
    reg [23:0]  data_reg;                   //寄存数据，每个十进制数对应4位的BCD码，六个数码管，所以是6*4=24，需要24位

    //数据的判断从最高位和符号位判断开始，如果最高位是0，则不显示，否则6个数码管全部点亮；对于次高位及之后的位也使用该逻辑
    //小数点也从最高位开始判断后的小数点开始判断
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            data_reg <= 24'd0;
        else if((h_thou) || point[5])
            data_reg <= {h_thou,t_thou,thou,hund,ten,unit};             //最高位之前是没有符号位的，这里不用判断符号位
        else if(((t_thou) || point[4]) && (sign == 1'b1))               //显示负号，定义4'd10负数，4'd11为正数；在后边的case语句选择对应的符号编码方式即可
            data_reg <= {4'd10,t_thou,thou,hund,ten,unit};              //若显示的十进制数的万位非零或要显示小数点，则点亮5个数码管；符号位也要判断
        else if(((t_thou) || point[4]) && (sign == 1'b0))               //正数，无符号
            data_reg <= {4'd11,t_thou,thou,hund,ten,unit};
        else if(((thou) || point[3]) && (sign == 1'b1))                 //负数，千位
            data_reg <= {4'd11,4'd10,thou,hund,ten,unit};               
        else if(((thou) || point[3]) && (sign == 1'b0))                 //正数，千位
            data_reg <= {4'd11,4'd11,thou,hund,ten,unit};               
        else if(((hund) || point[2]) && (sign == 1'b1))                 //负数，百位
            data_reg <= {4'd11,4'd11,4'd10,hund,ten,unit};               
        else if(((hund) || point[2]) && (sign == 1'b0))                 //正数，百位
            data_reg <= {4'd11,4'd11,4'd11,hund,ten,unit}; 
        else if(((ten) || point[1]) && (sign == 1'b1))                  //负数，十位
            data_reg <= {4'd11,4'd11,4'd11,4'd10,ten,unit}; 
        else if(((ten) || point[1]) && (sign == 1'b0))                  //正数，百位
            data_reg <= {4'd11,4'd11,4'd11,4'd11,ten,unit}; 
        else if(((unit) || point[0]) && (sign == 1'b1))                 //负数，个位
            data_reg <= {4'd11,4'd11,4'd11,4'd11,4'd10,unit};     
        else 
            data_reg <= {4'd11,4'd11,4'd11,4'd11,4'd11,unit};      
    end

    //1ms时间计时
    reg [15:0]  cnt_1ms;

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_1ms <= 16'd0;
        else if(cnt_1ms == CNT_1MS)
            cnt_1ms <= 16'd0;
        else 
            cnt_1ms <= cnt_1ms + 16'd1;
    end

    //每计数足够1ms产生一个标志信号脉冲
    reg flag_1ms;
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            flag_1ms <= 1'b0;
        else if(cnt_1ms == CNT_1MS - 1'd1)
            flag_1ms <= 1'b1;
        else
            flag_1ms <= 1'b0;
    end

    //对flag信号进行计数以控制数码管的位选信号
    reg [2:0]   cnt_flag;       //6个数码管，计数范围是0~5

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            cnt_flag <= 3'd0;
        else if((cnt_flag == 3'd5)&&(flag_1ms == 1'b1))
            cnt_flag <= 3'd0;
        else if(flag_1ms == 1'b1)
            cnt_flag <= cnt_flag + 3'd1;
        else
            cnt_flag <= cnt_flag;
    end

    //数码管位选信号寄存器
    reg [5:0]   sel_reg;

    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)  
            sel_reg <= 6'b0;
        else if((cnt_flag == 3'd0)&&(flag_1ms == 1'b1))
            sel_reg <= 6'b00_0001;              //为0时初始化选定第0个数码管
        else if(flag_1ms == 1'b1)
            sel_reg <= (sel_reg << 1'b1);       //当标志信号来临时左移一位选定下一个数码管
        else
            sel_reg <= sel_reg;                 //其他情况下保持不变以用于下一次移位状态
    end

    //点亮对应数码管的数据寄存器
    reg [3:0]   data_disp;
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            data_disp <= 4'd0;
        else if((seg_en == 1'b1)&&(flag_1ms == 1'b1))begin 
            case(cnt_flag)
                3'd0    : data_disp <= data_reg[3:0];      //个位，第0个数码管
                3'd1    : data_disp <= data_reg[7:4];      //十位，第1个数码管
                3'd2    : data_disp <= data_reg[11:8];     //百位，第2个数码管
                3'd3    : data_disp <= data_reg[15:12];    //千位，第3个数码管
                3'd4    : data_disp <= data_reg[19:16];    //万位，第4个数码管
                3'd5    : data_disp <= data_reg[23:20];    //十万位，第5个数码管
                default : data_disp <= 4'b0000;            //以上均不符合时，置零 
            endcase
        end
        else 
            data_disp <= data_disp;
    end

    //小数点信号控制，低有效，需要取反
    reg     dot_disp;
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            dot_disp <= 1'b0;
        else if(flag_1ms == 1'b1)
            dot_disp <= ~point[cnt_flag];
        else
            dot_disp <= dot_disp;
    end

    //数码管段选信号控制,显示数字0~9
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            seg <= 8'b1111_1111;        //默认熄灭不显示
        else begin 
            case(data_disp)     //这里的显示数字需要查找对应的码表
                4'd0 :  seg <= {dot_disp,7'b100_0000};   //显示数字 0    'h40
                4'd1 :  seg <= {dot_disp,7'b111_1001};   //显示数字 1    'h79
                4'd2 :  seg <= {dot_disp,7'b010_0100};   //显示数字 2    'h24
                4'd3 :  seg <= {dot_disp,7'b011_0000};   //显示数字 3
                4'd4 :  seg <= {dot_disp,7'b001_1001};   //显示数字 4
                4'd5 :  seg <= {dot_disp,7'b001_0010};   //显示数字 5
                4'd6 :  seg <= {dot_disp,7'b000_0010};   //显示数字 6
                4'd7 :  seg <= {dot_disp,7'b111_1000};   //显示数字 7
                4'd8 :  seg <= {dot_disp,7'b000_0000};   //显示数字 8
                4'd9 :  seg <= {dot_disp,7'b001_0000};   //显示数字 9
                4'd10:  seg <= 8'hBF;                    //显示负号
                4'd11:  seg <= 8'hFF;                    //不显示任何字符
                default:seg <= 8'b1100_0000;             //显示数字0 
            endcase
        end
    end

    //数码管位选信号赋值,打一拍以实现和seg的同步
    always@(posedge sys_clk or negedge sys_rst_n)begin 
        if(!sys_rst_n)
            sel <= 6'b000_000;
        else
            sel <= sel_reg;
    end

endmodule