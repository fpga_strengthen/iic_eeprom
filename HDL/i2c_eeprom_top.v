module i2c_eeprom_top(
    i_clk_50M,
    i_rst_n,
    i_key_wr,
    i_key_rd,
    o_shcp,
    o_stcp,
    o_oe,
    o_ds,
    o_scl,
    io_sda
);
//------------------端口声明-------------------------//
    input   wire    i_clk_50M;
    input   wire    i_rst_n;
    input   wire    i_key_wr;
    input   wire    i_key_rd;
    
    output  wire    o_shcp;
    output  wire    o_stcp;
    output  wire    o_oe;
    output  wire    o_ds;
    output  wire    o_scl;
    
    inout   wire    io_sda;

//------------------连线声明-------------------------//
    
    wire        key_wr_ctrl;
    wire        key_rd_ctrl;
    wire        i2c_clk;
    wire        i2c_end;
    wire [7:0]  eeprom_rd_data;
    wire        rd_en;
    wire        wr_en;
    wire [7:0]  fifo_data;
    wire        i2c_start;
    wire [15:0] rw_addr;
    wire [7:0]  wr_data;

//------------------参数声明-------------------------//
    
    parameter   CNT_MAX = 20'd999_999;

//------------------模块调用-------------------------//
    
    //按键消抖
    key_filter
    #(
        .CNT_MAX (CNT_MAX)          //计数器计数最大值
    )
    U_wr_key_filter
    (
        .sys_clk     (i_clk_50M),           //系统时钟50Mhz
        .sys_rst_n   (i_rst_n),             //全局复位
        .key_in      (i_key_wr),            //按键输入信号

        .key_flag    (key_wr_ctrl)          //消抖后的写操作按键控制信号
    );

    key_filter
    #(
        .CNT_MAX (CNT_MAX)                  //计数器计数最大值
    )
    U_rd_key_filter
    (
        .sys_clk     (i_clk_50M),           //系统时钟50Mhz
        .sys_rst_n   (i_rst_n),             //全局复位
        .key_in      (i_key_rd),            //按键输入信号

        .key_flag    (key_rd_ctrl)          //消抖后的读操作按键控制信号
    );

    i2c_rw_ctrl U_i2c_rw_ctrl(
        .i_clk_50M    (i_clk_50M),
        .i_i2c_clk    (i2c_clk),
        .i_rst_n      (i_rst_n),
        .i_write      (key_wr_ctrl),
        .i_read       (key_rd_ctrl),
        .i_i2c_end    (i2c_end),
        .iv_rd_data   (eeprom_rd_data),

        .o_rd_en      (rd_en),
        .o_wr_en      (wr_en),
        .ov_fifo_data (fifo_data),
        .o_i2c_start  (i2c_start),
        .ov_rw_addr   (rw_addr),
        .ov_wr_data   (wr_data)
    );

    i2c_ctrl
    #(
        .DEVICE_ADDR (7'b1010_011   ),              //i2c设备地址
        .SYS_CLK_FREQ(26'd50_000_000),              //输入系统时钟频率，50MHz
        .SCL_FREQ    (18'd250_000   )               //i2c设备scl时钟频率,250kHz
    )
    U_i2c_ctrl
    (
        .i_clk_50M     (i_clk_50M),
        .i_rst_n       (i_rst_n),
        .i_i2c_start   (i2c_start),
        .i_wr_en       (wr_en),
        .i_rd_en       (rd_en),
        .iv_byte_addr  (rw_addr),
        .iv_wr_data    (wr_data),
        .i_addr_num    (1'b1),

        .io_sda        (io_sda),
        
        .o_scl         (o_scl),
        .ov_rd_data    (eeprom_rd_data),
        .o_i2c_clk     (i2c_clk),
        .o_i2c_end     (i2c_end)
    );

    seg_595_dynamic U_seg_dynamic(
        .sys_clk       (i_clk_50M),
        .sys_rst_n     (i_rst_n),
        .data          (fifo_data),
        .point         (6'b0),
        .sign          (1'b0),
        .seg_en        (1'b1),
        .ds            (o_ds),
        .oe            (o_oe),
        .shcp          (o_shcp),
        .stcp          (o_stcp)
    );

endmodule