//-----------------------------------------------------------------------------
//注意：sda线是inout类型端口，是wire类型的，因此要在合适的时序对其输出或输入进行配置
//最好的方法是分别将其输出前的和输入后的信号寄存，在合适的时间用assign赋值或取值
//------------------------------------------------------------------------------
module i2c_ctrl
#(
    parameter   DEVICE_ADDR  = 7'b1010_011,             //i2c设备地址
    parameter   SYS_CLK_FREQ = 26'd50_000_000,          //输入系统时钟频率，50MHz
    parameter   SCL_FREQ     = 18'd250_000              //i2c设备scl时钟频率,250kHz
)
(
    i_clk_50M,
    i_rst_n,
    i_i2c_start,
    i_wr_en,
    i_rd_en,
    iv_byte_addr,
    iv_wr_data,
    i_addr_num,
    io_sda,
    o_scl,
    ov_rd_data,
    o_i2c_clk,
    o_i2c_end
);
//------------------------------输入输出端口声明------------------------------------//
    input   wire        i_clk_50M;                  //系统时钟，50M
    input   wire        i_rst_n;                    //复位，低有效
    input   wire        i_i2c_start;                //i2c读写控制的开始标志
    input   wire        i_wr_en;                    //写使能
    input   wire        i_rd_en;                    //读使能
    input   wire [15:0] iv_byte_addr;               //地址字节
    input   wire [7:0]  iv_wr_data;                 //要写入的数据字节
    input   wire        i_addr_num;                 //地址长度标志，1表示2字节，表示低字节；当前EEPROM的地址是2字节，所以恒为高电平

    output  reg         o_scl;                      //输出的i2c串行时钟SCL，频率250kHz
    output  reg  [7:0]  ov_rd_data;                 //eeprom读出的数据转化为字节
    output  wire        o_i2c_clk;                  //输出给i2c读写控制模块的时钟
    output  reg         o_i2c_end;                  //i2c读写操作结束标志

    inout   wire        io_sda;                     //数据传输的串行比特数据线，双向数据线

//-----------------------------寄存器、连线和常量声明------------------------------------//
    
    //要保证输出的i2c_clk是scl时钟频率的4倍，先得到1M的时钟频率
    //sys_clk_freq/scl_freq表示系统时钟对于scl时钟的时钟周期倍数，此时为200
    //例如要进行50分频，计数范围应当是25个，即0~24，一个周期计数50个，高低电平各25个，就实现了50分频
    localparam  CNT_CLK_MAX  = (SYS_CLK_FREQ / SCL_FREQ) >> 2'd3;

    //状态机状态变量声明
    localparam  IDLE         = 4'd0,                //空闲状态
                START_1      = 4'd1,                //开始发送(写)状态
                SEND_DADDR   = 4'd2,                //发送设备地址 Send Device Address
                ACK_1        = 4'd3,                //从机发送应答信号给FPGA
                SEND_BADDR_H = 4'd4,                //发送地址的高字节
                ACK_2        = 4'd5,                //从机发送应答信号给FPGA
                SEND_BADDR_L = 4'd6,                //发送地址的低字节
                ACK_3        = 4'd7,                //从机发送应答信号给FPGA
                WR_DATA      = 4'd8,                //写数据
                ACK_4        = 4'd9,                //从机发送应答信号给FPGA
                START_2      = 4'd10,               //开始读状态
                SEND_RD_ADDR = 4'd11,               //发送读地址
                ACK_5        = 4'd12,               //从机发送应答信号给FPGA
                RD_DATA      = 4'd13,               //读数据状态
                NO_ACK       = 4'd14,               //从机发送应答信号给FPGA
                STOP         = 4'd15;               //停止状态

    reg [3:0]   state;                              //状态机变量
    reg         cnt_i2c_clk_en;                     //当状态机状态为读写状态时该信号就持续拉高,控制计数器的计数
    reg [4:0]   cnt_clk_50M;                        //对系统时钟计数(0~24)，进行50分频，得到1M的时钟
    reg         clk_1M;                             //1M时钟
    reg [1:0]   cnt_clk_1M;                         //对1M的时钟计数 
    reg [2:0]   cnt_bit;                            //对发送的串行比特数据进行计数，发送给sda_out
    reg         sda_out;                            //io_sda是wire类型的，所以用sda_out先做数据缓存
    wire        ack_state;                          //应答状态
    reg         ack;                                //接收从机的应答信号并缓存
    wire        sda_en;                             //串行数据的控制信号
    reg [7:0]   rd_data_reg;                        //缓存读出的数据
    wire        sda_in;                             //和io_sda一致，作为对输入信号的采样
    wire        send_state;                         //所有发送状态任一成立即拉高

//-----------------------------------------------------------------------------------//                

    //当状态机状态为读写状态时该信号就持续拉高
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_i2c_clk_en <= 1'b0;
        else if(i_i2c_start == 1'b1)
            cnt_i2c_clk_en <= 1'b1;                     //当读写控制信号有效时，就拉高计数器使能信号
        else if((state == STOP)&&(cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd2))
            cnt_i2c_clk_en <= 1'b0;                     //不需要列出中间的所有情况去分别判断，只需要对开始和结束时做处理，中间会一直保持
        else
            cnt_i2c_clk_en <= cnt_i2c_clk_en;
    end

    //对系统时钟计数
    always @(posedge i_clk_50M or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            cnt_clk_50M <= 5'd0;
        else if(cnt_clk_50M == CNT_CLK_MAX - 1'b1)
            cnt_clk_50M <= 5'd0;                        //50分频，计数范围0~24，每到24取反一次，一个周期的长度为(0~24)*2=50，得到1M时钟信号
        else
            cnt_clk_50M <= cnt_clk_50M + 5'd1;          //持续计数
    end

    //根据计数器cnt_clk得到1M的时钟信号
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            clk_1M <= 1'b0;
        else if(cnt_clk_50M == CNT_CLK_MAX - 1'b1)
            clk_1M <= ~clk_1M;
        else
            clk_1M <= clk_1M;
    end

    assign  o_i2c_clk = clk_1M;

    //对1M的时钟clk_1M计数分频得到250kHz的时钟i2c_clk
    always@(posedge clk_1M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_clk_1M <= 2'd0;
        else if(cnt_i2c_clk_en == 1'b1)
            cnt_clk_1M <= cnt_clk_1M + 2'd1;            //只在使能信号有效时进行计数，计数可以分频得到250kHz时钟
        else
            cnt_clk_1M <= cnt_clk_1M;
    end

    //将所有应答状态写在一起
    assign  ack_state = (state == ACK_1)||(state == ACK_2)||(state == ACK_3)||(state == ACK_4)||(state == ACK_5);
    //所有发送状态
    assign  send_state = (state == SEND_DADDR)||(state == SEND_BADDR_H)||(state == SEND_BADDR_L)||(state == SEND_RD_ADDR);


    //cnt_bit计数器，对串行比特计数，控制输出
    always@(posedge clk_1M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if((state == START_1)||(ack_state == 1'b1)||(state == NO_ACK)||(state == IDLE)||(state == START_2))
            cnt_bit <= 3'd0;                            //只在读写字节数据时才对比特计数
        else if((cnt_bit == 3'd7)&&(cnt_clk_1M == 2'd3)) 
            cnt_bit <= 3'd0;                            //不能加满就自己清零，否则数据长度不够
        else if((cnt_clk_1M == 2'd3)&&(state != IDLE))
            cnt_bit <= cnt_bit + 3'd1;
        else    
            cnt_bit <= cnt_bit;
    end

//--------------------------------------状态机跳转控制-----------------------------------//
    always@(posedge clk_1M or negedge i_rst_n)begin             //posedge i_clk_50M
        if(i_rst_n == 1'b0)
            state <= IDLE;
        else begin 
            case(state)
                IDLE : begin                    //0
                    if(i_i2c_start == 1'b1)
                        state <= START_1;
                    else    
                        state <= state;
                end
                START_1 : begin                 //1
                    if(cnt_clk_1M == 2'd3)
                        state <= SEND_DADDR;
                    else
                        state <= state;
                end
                SEND_DADDR : begin               //2
                    if((cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd7))
                        state <= ACK_1;
                    else 
                        state <= state;
                end
                ACK_1 : begin                    //3
                    if((cnt_clk_1M == 2'd3)&&(ack == 1'b0))begin 
                        if(i_addr_num == 1'b1)
                            state <= SEND_BADDR_H;                  //发送存储地址高字节
                        else
                            state <= SEND_BADDR_L;                  //发送存储地址低字节
                    end
                    else
                        state <= state;
                end
                SEND_BADDR_H : begin            //4
                    if((cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd7))
                        state <= ACK_2;
                    else
                        state <= state;
                end
                ACK_2 : begin                   //5
                    if((cnt_clk_1M == 2'd3)&&(ack == 1'b0))
                        state <= SEND_BADDR_L;
                    else
                        state <= state;
                end
                SEND_BADDR_L : begin            //6
                    if((cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd7))
                        state <= ACK_3;
                    else
                        state <= state;
                end
                ACK_3 : begin                   //7
                    //判断的条件前两个重复时，不要写成 if((cnt_clk_1M == 2'd3)&&(ack == 1'b0)&&(i_wr_en == 1'b1))去重复判断
                    if((cnt_clk_1M == 2'd3)&&(ack == 1'b0))begin
                        if(i_wr_en == 1'b1)
                            state <= WR_DATA;
                        else if(i_rd_en == 1'b1)
                            state <= START_2;
                        else
                            state <= state;
                    end
                    else
                        state <= state;
                end
                WR_DATA : begin                 //8
                    if((cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd7))
                        state <= ACK_4;
                    else 
                        state <= state;
                end
                ACK_4 : begin                   //9
                    if((cnt_clk_1M == 2'd3)&&(ack == 1'b0))
                        state <= STOP;
                    else
                        state <= state;
                end
                START_2 : begin                 //10
                    if(cnt_clk_1M == 2'd3)
                        state <= SEND_RD_ADDR;              //START_2状态期间，发送一个起始信号，长度为一个scl周期；之后跳转到发送读控制指令状态
                    else
                        state <= state;
                end
                SEND_RD_ADDR : begin            //11
                    if((cnt_bit == 3'd7)&&(cnt_clk_1M == 2'd3))
                        state <= ACK_5;                     //读控制指令发送完成时，等待从机应答
                    else
                        state <= state;
                end
                ACK_5 : begin                   //12
                    if((cnt_clk_1M == 2'd3)&&(ack == 1'b0))
                        state <= RD_DATA;
                    else
                        state <= state;
                end
                RD_DATA : begin                 //13
                    if((cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd7))
                        state <= NO_ACK;
                    else
                        state <= state;
                end
                NO_ACK : begin                  //14
                    if(cnt_clk_1M == 2'd3)
                        state <= STOP;
                    else
                        state <= state;
                end
                STOP : begin                    //15
                    //完成数据读写后,串口时钟SCL保持高电平，当串口数据信号SDA产生一个由低电平转为高电平的上升沿时，产生一个停止信号
                    if((cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd2))
                        state <= IDLE;
                    else
                        state <= state;
                end
                default: state <= IDLE;
            endcase
        end
    end

//-----------------------------向从设备写入控制指令(地址+读写控制位)------------------------------//
    
    //串行时钟信号scl
    //scl的高电平对应数据的中心，低电平对应数据的更新（跳变），特别注意scl和数据不要同时跳变，否则会导致采样错误
    //SDA上的数据在SCL为高电平时写入从机设备
    //在开始状态以后，停止状态之前，主从机有数据的写入或读出的交互时，都需要有scl作为操作时钟
    always@(*)begin 
        if(state == IDLE)
            o_scl = 1'b1;
        else if(state == START_1)begin
            if(cnt_clk_1M == 2'd3)
                o_scl = 1'b0;
            else
                o_scl = 1'b1;
        end
        else if((ack_state == 1'b1)||(state == RD_DATA)||(send_state == 1'b1)||(state == WR_DATA)||(state == START_2))begin 
            if((cnt_clk_1M == 2'd1) || (cnt_clk_1M == 2'd2))
                o_scl = 1'b1;
            else
                o_scl = 1'b0;
        end
        else if(state == NO_ACK)begin 
            if((cnt_clk_1M == 2'd1) || (cnt_clk_1M == 2'd2))
                o_scl = 1'b1;
            else                        //无应答状态也要产生正确的SCL时钟信号，此状态下SDA为常高状态
                o_scl = 1'b0;
        end
        else if(state == STOP)
            o_scl = 1'b1;              //停止状态时，SCL持续拉高，SDA产生由低到高的电平变化表示停止位
        else
            o_scl = 1'b1;              //空闲状态为高电平
    end

    //always@(*)begin 
    //    case(state)
    //        IDLE :
    //            o_scl <= 1'b1;
    //        START_1 : 
    //            if(cnt_clk_1M == 2'd3)
    //                o_scl <= 1'b0;
    //            else
    //                o_scl <= 1'b1;
    //        SEND_DADDR,ACK_1,SEND_BADDR_H,ACK_2,SEND_BADDR_L,ACK_3,
    //        WR_DATA,ACK_4,START_2,SEND_RD_ADDR,ACK_5,RD_DATA,NO_ACK:
    //            if((cnt_clk_1M == 2'd1)||(cnt_clk_1M == 2'd2))
    //                o_scl <= 1'b1;
    //            else
    //                o_scl <= 1'b0;
    //        STOP : 
    //            if((cnt_bit == 3'd0)&&(cnt_clk_1M == 2'd0))
    //                o_scl <= 1'b0;
    //            else
    //                o_scl <= 1'b1;
    //        default:o_scl <= 1'b1;
    //    endcase
    //end

    //SDA_reg
    always@(*)begin 
        if(state == START_1)begin 
            if(cnt_clk_1M == 2'd0)                          
                sda_out <= 1'b1;
            else                    //在scl为高时，将sda信号拉低作为发送的起始信号
                sda_out <= 1'b0;
        end                             
        else if(state == SEND_DADDR)begin   //发送控制指令(器件地址+写控制位)
            if(cnt_bit <= 3'd6)
                sda_out <= DEVICE_ADDR[6 - cnt_bit];          //先发送器件地址
            else
                sda_out <= 1'b0;                            //发送写控制位
        end
        else if(state == ACK_1)
            sda_out <= 1'b1;                                //应答状态时空闲，设置为高
        else if(state == SEND_BADDR_H)
            sda_out <= iv_byte_addr[15 - cnt_bit];          //发送存储地址高字节
        else if(state == ACK_2)
            sda_out <= 1'b1;                                //与ACK_1同理
        else if(state == SEND_BADDR_L)
            sda_out <= iv_byte_addr[7 - cnt_bit];           //发送存储地址低字节
        else if(state == ACK_3)
            sda_out <= 1'b1;                                //与ACK_1同理
        else if(state == WR_DATA)
            sda_out <= iv_wr_data[7 - cnt_bit];             //写入单字节数据
        else if(state == ACK_4)
            sda_out <= 1'b1;                                //与ACK_1同理
        else if(state == START_2)begin                    
            if(cnt_clk_1M <= 2'd1)                      
                sda_out <= 1'b1;
            else                    //在scl为高时，将sda产生一个从高到低的电平变化，表示起始信号
                sda_out <= 1'b0;
        end                 
        else if(state == SEND_RD_ADDR)begin 
            if(cnt_bit <= 3'd6) 
                sda_out <= DEVICE_ADDR[6-cnt_bit];          //发送器件地址
            else
                sda_out <= 1'b1;                            //发送读控制位
        end
        else if(state == ACK_5)
            sda_out <= 1'b1;                                //与ACK_1同理
        else if(state == NO_ACK)
            sda_out <= 1'b1;                                //写入一个时钟的高电平
        else if(state == STOP)begin 
            if((cnt_bit == 3'd0)&&(cnt_clk_1M <= 2'd2))
                sda_out <= 1'b0;
            else                                            //在scl为高时,sda产生一个从低到高变化的电平，表示停止信号
                sda_out <= 1'b1;
        end
        else
            sda_out <= 1'b1;
    end

    //sda_en在io_sda作为输出时拉高，作为输入时拉低
    assign  sda_en = ((ack_state == 1'b1)||(state == RD_DATA)) ? 1'b0:1'b1;

    //当sda_en为高时，表示当前为输出状态；否则为高阻态
    assign  io_sda = (sda_en == 1'b1) ? sda_out : 1'bz;

    //sda_in和io_sda完全相同
    assign  sda_in = io_sda;                    
    
    //在应答状态时接收io_sda回传的应答信号
    always@(*)begin 
        if((ack_state == 1'b1))begin 
            if(cnt_clk_1M == 2'd0)
                ack <= sda_in; 
            else 
                ack <= ack;
        end
        else
            ack <= 1'b1;
    end

    //将接收的从机发送的比特数据存储
    //这里采用组合逻辑，有移位拼接操作，不要使用非阻塞赋值，效果和下面一段注释掉代码的结果是一样的
    always@(*)begin 
        if(i_rst_n == 1'b0)
            rd_data_reg = 8'b0000_0000;
        else if((state == RD_DATA)&&(cnt_clk_1M == 2'd2))
            rd_data_reg = {rd_data_reg[6:0],sda_in};
        else
            rd_data_reg = rd_data_reg;
    end

    //always@(*)begin 
    //    case(state)
    //        IDLE : 
    //            rd_data_reg <= 8'b0000_0000;
    //        RD_DATA : 
    //            if(cnt_clk_1M == 2'd2)
    //                //rd_data_reg <= {rd_data_reg[6:0],sda_in};
    //                rd_data_reg[7-cnt_bit] <= sda_in;
    //            else
    //                rd_data_reg <= rd_data_reg;
    //        default : 
    //            rd_data_reg <= rd_data_reg;
    //    endcase
    //end

    //当接收到一个完整字节时，将暂存的字节数据发送
    always@(posedge clk_1M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            ov_rd_data <= 8'b0;
        else if((state == RD_DATA)&&(cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd7))
            ov_rd_data <= rd_data_reg;
        else
            ov_rd_data <= ov_rd_data;
    end

    always@(posedge clk_1M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_i2c_end <= 1'b0;
        else if((state == STOP)&&(cnt_clk_1M == 2'd3)&&(cnt_bit == 3'd2))
            o_i2c_end <= 1'b1;
        else
            o_i2c_end <= 1'b0;
    end

endmodule