//本模块的主时钟是1MHz,50MHz用于跨时钟域操作
module i2c_rw_ctrl(
    i_clk_50M,
    i_i2c_clk,
    i_rst_n,
    i_write,
    i_read,
    i_i2c_end,
    iv_rd_data,
    o_rd_en,
    o_wr_en,
    ov_fifo_data,
    o_i2c_start,
    ov_rw_addr,
    ov_wr_data
);
    input   wire        i_clk_50M;                  //系统时钟，50MHz
    input   wire        i_i2c_clk;                  //i2c操作时钟，1MHz
    input   wire        i_rst_n;                    //复位，低有效
    input   wire        i_write;                    //控制写操作的按键信号经消抖后传入，时钟50M
    input   wire        i_read;                     //控制读操作的按键信号经消抖后传入，时钟50M
    input   wire        i_i2c_end;                  //I2C一个字节数据的读写操作完毕标志，时钟1M
    input   wire [7:0]  iv_rd_data;                 //从EEPROM读出的字节数据

    output  reg         o_rd_en;                    //读使能信号，用来驱动i2c_ctrl模块
    output  reg         o_wr_en;                    //写使能信号，用来驱动i2c_ctrl模块
    output  reg         o_i2c_start;                //控制i2c读写操作开始的标志信号
    output  reg  [15:0] ov_rw_addr;                 //读写操作的地址，有高低两个字节，16位
    output  reg  [7:0]  ov_wr_data;                 //待写入EEPROM的数据
    output  wire [7:0]  ov_fifo_data;               //由FIFO读出的字节

//-----------------------------------参数、寄存器和连线声明-----------------------------------//

    parameter   MAX_CNT_CLK_50M  = 8'd150;          //跨时钟域计数器
    parameter   MAX_WRITE_NUM    = 6'd5,           //写入的字节数量
                MAX_READ_NUM     = 6'd5;           //读出的字节数量
    parameter   MAX_CNT_DELAY    = 13'd5000;        //读写操作时间间隔
    parameter   RW_ADDR_INIT     = 16'h00_22,       //读/写地址初始化
                WR_DATA_INIT     = 8'hAA;           //写数据初始化
    parameter   MAX_FIFO_RD_WAIT = 19'd49_9999;     //FIFO读等待时间最大值，约0.5s
    
    reg     [7:0]   cnt_clk_50M;                    //50MHz同步到1MHz，至少需要延时100个时钟周期，为了保险起见，延时150个
    reg             write_valid;                    //写有效信号
    reg             read_valid;                     //读有效信号
    reg     [5:0]   cnt_wr_num;                     //对写入完成的字节计数，写入完成时拉低写使能
    reg     [12:0]  cnt_rw_delay;                   //控制读写操作有效信号i2c_start的间隔
    reg     [5:0]   cnt_rd_num;                     //对读出的字节计数
    reg             fifo_rd_valid;                  //FIFO读有效信号
    reg             fifo_rd_en;                     //FIFO读使能信号
    wire    [5:0]   fifo_data_num;                  //FIFO中的数据数量
    reg     [5:0]   cnt_fifo_rd_num;                //对读出FIFO的数据计数，读完时拉低读有效信号
    reg     [18:0]  cnt_fifo_rd_wait;               //fifo读等待时间计数器
    wire            fifo_wr_en;

//----------------------------------跨时钟域--------------------------------------------------//

    //将50M下的按键触发信号同步到1M时钟下，延长时间宽度

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_clk_50M <= 8'd0;
        else if(cnt_clk_50M == MAX_CNT_CLK_50M)
            cnt_clk_50M <= 8'd0;
        else if((i_write == 1'b1)||(i_read == 1'b1))
            cnt_clk_50M <= 8'd1;                    //计数起点是1，因此计数到最大值无需减1
        else if(cnt_clk_50M > 0)
            cnt_clk_50M <= cnt_clk_50M + 8'd1;
        else
            cnt_clk_50M <= cnt_clk_50M;
    end

    //将50M宽度的i_write延长到1M宽度
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            write_valid <= 1'b0;
        else if(cnt_clk_50M == MAX_CNT_CLK_50M)
            write_valid <= 1'b0;
        else if(i_write == 1'b1)
            write_valid <= 1'b1;
        else 
            write_valid <= write_valid;
    end

    //对于读触发信号同理，复用写操作的计数器
    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            read_valid <= 1'b0;
        else if(cnt_clk_50M == MAX_CNT_CLK_50M)
            read_valid <= 1'b0;
        else if(i_read == 1'b1)
            read_valid <= 1'b1;
        else 
            read_valid <= read_valid;
    end

//--------------------根据跨时钟域后的标志信号处理读写操作--------------------------//

    //-------------------------写操作部分------------------------------//

    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_wr_num <= 6'd0;
        else if((cnt_wr_num == MAX_WRITE_NUM - 1'b1)&&(i_i2c_end == 1'b1))
            cnt_wr_num <= 6'd0;
        else if((i_i2c_end == 1'b1)&&(o_wr_en == 1'b1))
            cnt_wr_num <= cnt_wr_num + 6'd1;
        else
            cnt_wr_num <= cnt_wr_num;
    end

    //当写信号有效时，拉高写使能信号，直到写操作结束
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_wr_en <= 1'b0;
        else if(write_valid == 1'b1)
            o_wr_en <= 1'b1;
        else if((cnt_wr_num == MAX_WRITE_NUM - 1'b1)&&(i_i2c_end == 1'b1))
            o_wr_en <= 1'b0;
        else
            o_wr_en <= o_wr_en;
    end

    //控制读写操作有效信号i2c_start的间隔，根据数据手册，写操作需要5ms，因此至少间隔5ms
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_rw_delay <= 13'd0;
        else if(cnt_rw_delay == MAX_CNT_DELAY - 1'b1)
            cnt_rw_delay <= 13'd0;
        else if((o_wr_en == 1'b1)||(o_rd_en == 1'b1))
            cnt_rw_delay <= cnt_rw_delay + 13'd1;                   //复用该计数器，在读/写使能有效期间，持续计数
        else
            cnt_rw_delay <= 13'd0;
    end

    //读/写操作地址
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            ov_rw_addr <= RW_ADDR_INIT;                             //初始化地址
        else if(((cnt_wr_num == MAX_WRITE_NUM - 1'b1)||(cnt_rd_num == MAX_READ_NUM - 1'b1))&&(i_i2c_end == 1'b1))
            ov_rw_addr <= RW_ADDR_INIT;                             //当全部写入完毕时，地址归为初始地址
        else if(i_i2c_end == 1'b1)begin 
            if((o_wr_en == 1'b1)||(o_rd_en == 1'b1))
                ov_rw_addr <= ov_rw_addr + 16'd1;
            else
                ov_rw_addr <= ov_rw_addr;
        end
        else
            ov_rw_addr <= ov_rw_addr;
    end

    //写数据
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            ov_wr_data <= WR_DATA_INIT;
        else if((cnt_wr_num == MAX_WRITE_NUM - 1'b1)&&(i_i2c_end == 1'b1))
            ov_wr_data <= WR_DATA_INIT;
        else if((i_i2c_end == 1'b1)&&(o_wr_en == 1'b1))
            ov_wr_data <= ov_wr_data + 8'b1;                        //数据和地址的变化是同步的，都是由有效的i2c_end信号触发的
        else 
            ov_wr_data <= ov_wr_data;
    end

    //--------------------------读操作部分-----------------------------//

    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_rd_num <= 6'd0;
        else if((cnt_rd_num == MAX_READ_NUM - 1'b1)&&(i_i2c_end == 1'b1))
            cnt_rd_num <= 6'd0;
        else if((i_i2c_end == 1'b1)&&(o_rd_en == 1'b1))
            cnt_rd_num <= cnt_rd_num + 1'b1;
        else
            cnt_rd_num <= cnt_rd_num;
    end

    //读使能
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_rd_en <= 1'b0;
        else if(read_valid == 1'b1)
            o_rd_en <= 1'b1;
        else if((cnt_rd_num == MAX_READ_NUM - 1'b1)&&(i_i2c_end == 1'b1))
            o_rd_en <= 1'b0;
        else
            o_rd_en <= o_rd_en;
    end

    //---------------------读写操作开始控制信号-------------------------//
    //持续时间为一个1M周期
    always@(posedge i_i2c_clk or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            o_i2c_start <= 1'b0;
        else if(cnt_rw_delay == MAX_CNT_DELAY - 1'b1)
            o_i2c_start <= 1'b1;
        else 
            o_i2c_start <= 1'b0;
    end

//------------------------------数据缓存FIFO--------------------------------//

    //fifo读有效控制信号
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            fifo_rd_valid <= 1'b0;
        else if((cnt_rd_num == MAX_READ_NUM - 1'b1)&&(i_i2c_end == 1'b1))
            fifo_rd_valid <= 1'b1;
        else if((cnt_fifo_rd_num == MAX_READ_NUM)&&(cnt_fifo_rd_wait == MAX_FIFO_RD_WAIT))
            fifo_rd_valid <= 1'b0;
        else 
            fifo_rd_valid <= fifo_rd_valid;
    end

    //FIFO读使能时间间隔
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_fifo_rd_wait <= 19'd0;
        else if(cnt_fifo_rd_wait == MAX_FIFO_RD_WAIT)
            cnt_fifo_rd_wait <= 19'd0;
        else if(fifo_rd_valid == 1'b1)
            cnt_fifo_rd_wait <= cnt_fifo_rd_wait + 1'b1;
        else
            cnt_fifo_rd_wait <= cnt_fifo_rd_wait; 
    end

    //fifo读使能信号
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            fifo_rd_en <= 1'b0;
        else if(cnt_fifo_rd_wait == MAX_FIFO_RD_WAIT)
            fifo_rd_en <= 1'b1;
        else
            fifo_rd_en <= 1'b0;
    end
    
    //对FIFO读出的数据计数
    always@(posedge i_i2c_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_fifo_rd_num <= 6'd0;
        else if(fifo_rd_valid == 1'b0)
            cnt_fifo_rd_num <= 6'd0;
        else if(fifo_rd_en == 1'b1)
            cnt_fifo_rd_num <= cnt_fifo_rd_num + 6'd1;
        else 
            cnt_fifo_rd_num <= cnt_fifo_rd_num;
    end

    //fifo写使能：只在读使能有效状态下（此时才会从EEPROM读出数据）
    assign      fifo_wr_en = (o_rd_en == 1'b1)&&(i_i2c_end == 1'b1);

    //FIFO实例化调用IP
    fifo_8x64 U_fifo_8x64(
	    .clock  (i_i2c_clk),
	    .data   (iv_rd_data),
	    .rdreq  (fifo_rd_en),
	    .wrreq  (fifo_wr_en),

	    .q      (ov_fifo_data),
	    .usedw  (fifo_data_num)
    );

endmodule