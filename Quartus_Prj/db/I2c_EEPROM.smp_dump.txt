
State Machine - |i2c_eeprom_top|i2c_ctrl:U_i2c_ctrl|state
Name state.STOP state.NO_ACK state.RD_DATA state.ACK_5 state.SEND_RD_ADDR state.START_2 state.ACK_4 state.WR_DATA state.ACK_3 state.SEND_BADDR_L state.ACK_2 state.SEND_BADDR_H state.ACK_1 state.SEND_DADDR state.START_1 state.IDLE 
state.IDLE 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
state.START_1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 
state.SEND_DADDR 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 1 
state.ACK_1 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 
state.SEND_BADDR_H 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 
state.ACK_2 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 1 
state.SEND_BADDR_L 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 
state.ACK_3 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 
state.WR_DATA 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 
state.ACK_4 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 1 
state.START_2 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 1 
state.SEND_RD_ADDR 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 1 
state.ACK_5 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 1 
state.RD_DATA 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 1 
state.NO_ACK 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
state.STOP 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 
